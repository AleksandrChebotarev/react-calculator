const getResult = (arrayValue, arrayOperand) => {
  for (let key = 0; key < arrayOperand.length; key++) {
    if (arrayOperand[key] === "*") {
      let res = arrayValue[key] * arrayValue[key + 1];
      arrayValue.splice(key, 2, res);
      arrayOperand.splice(key, 1);
      key--;
    }
    if (arrayOperand[key] === "/") {
      let res = arrayValue[key] / arrayValue[key + 1];
      arrayValue.splice(key, 2, res);
      arrayOperand.splice(key, 1);
      key--;
    }
  }
  for (let key = 0; key < arrayOperand.length; key++) {
    if (arrayOperand[key] === "-") {
      let res = arrayValue[key] - arrayValue[key + 1];
      arrayValue.splice(key, 2, res);
      arrayOperand.splice(key, 1);
      key--;
    }
    if (arrayOperand[key] === "+") {
      const res = arrayValue[key] + arrayValue[key + 1];
      arrayValue.splice(key, 2, res);
      arrayOperand.splice(key, 1);
      key--;
    }
  }
  return arrayValue[0];
};

const getHandler = (typeBtn, id, value, addDigit, reset, addOps, getResult) => {
  if (typeBtn === "btn-ops" && id === "result") {
    return getResult();
  }
  if (typeBtn === "btn-ops" && id === "reset") {
    return reset();
  }
  if (typeBtn === "btn-ops") {
    return addOps(value);
  }
  return addDigit(value);
};

export {getResult, getHandler};