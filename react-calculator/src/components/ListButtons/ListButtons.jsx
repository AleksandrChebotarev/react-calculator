import React from "react";
import Button from "../Button/Button";
import PropTypes from "prop-types";
import { getHandler } from "../../utils";

const ListButtons = ({ list, addDigit, reset, addOps, getResult }) => {
  return list.map((item) => (
    <Button
      key={item.id}
      className={item.styleBtn}
      onClick={() => getHandler(item.typeBtn, item.id, item.value, addDigit, reset, addOps, getResult)}
    >
      {item.value}
    </Button>
  ));
};

ListButtons.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      typeBtn: PropTypes.string,
      value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      styleBtn: PropTypes.string,
    })
  ),
  addDigit: PropTypes.func,
};

export default ListButtons;
