import { useState } from "react";
import PropTypes from "prop-types";
import "./index.css";
import { getResult } from "../../utils";
import ListButtons from "../ListButtons/ListButtons";

const Calculator = ({ listButtons }) => {
  const [calc, setCalc] = useState(0);
  const [num, setNum] = useState(0);
  const [arrayOperand, setArrayOperand] = useState([]);
  const [arrayValue, setArrayValue] = useState([]);
  const [display, setDisplay] = useState("");

  const handleAddDigit = (value) => {
    if (value === 0 && arrayOperand?.slice(-1)[0] === "/") {
      alert("На ноль делить нельзя!");
      return;
    }
    setDisplay(display + value);
    return setNum(Number("" + num + value));
  };

  const handleReset = () => {
    setCalc(0);
    setNum(0);
    setDisplay("");
    setArrayOperand([]);
    setArrayValue([]);
    return;
  };

  const handleAddOps = (value) => {
    (!calc) ? setArrayValue(arrayValue.concat([num])) : setCalc(0);
    (!display) ? setDisplay("0" + value) : setDisplay(display + value);
    setArrayOperand(arrayOperand.concat([value]));
    setNum(0);
    return;
  };

  const handleGetResult = () => {
    const result = getResult(arrayValue.concat([num]), arrayOperand);

    setArrayValue([result]);
    setArrayOperand([]);
    setNum(result);
    setDisplay(result);
    return setCalc(result);
  };

  return (
    <>
      <span className={"App-result"}>{display ? display : 0}</span>
      <div className={"App-container-buttons"}>
        <ListButtons
          list={listButtons}
          addDigit={handleAddDigit}
          reset={handleReset}
          addOps={handleAddOps}
          getResult={handleGetResult}
        />
      </div>
    </>
  );
};

Calculator.propTypes = {
  listButtons: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      typeBtn: PropTypes.string,
      value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      styleBtn: PropTypes.string,
    })
  ),
};

export default Calculator;
