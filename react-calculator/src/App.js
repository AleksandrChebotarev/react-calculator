import "./App.css";
import Calculator from "./components/Calculator/Calculator";
import { store } from "./store/store";

function App() {
  const { listButtons } = store;
  return (
    <div className="App">
      <Calculator listButtons={listButtons} />
    </div>
  );
}

export default App;
