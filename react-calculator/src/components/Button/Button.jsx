import React from "react";
import PropTypes from "prop-types";
import "./index.css";

const Button = ({ onClick, className, children }) => {
  return (
    <button className={className} onClick={onClick}>
      {children}
    </button>
  );
};

Button.propTypes = {
  onClick: PropTypes.func,
  styleBtn: PropTypes.string,
  children: PropTypes.any,
};

export default Button;
